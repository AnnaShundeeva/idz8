import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {

    /*Дан список объектов произвольных типов. Найдите количество элементов списка, которые
    являются объектами типа Human или его подтипами*/
    public static int findQuantityHuman(List<Object> objects) {
        int count = 0;
        for (Object o: objects) {
            if (o instanceof Human) {
                count++;
            }
        }
        return count;
    }

    //Для объекта получить список имен его открытых методов
    public static List<String> getNameOpenMethod (Object o) {
        List<String> names = new ArrayList<String>();

        Method[] methods = o.getClass().getMethods();
        for (Method method: methods) {
            names.add(method.getName());
        }
        return names;
    }

    /*Для объекта получить список (в виде списка строк) имен всех его суперклассов до класса
    Object включительно*/

    public static List<String> getAllSuperclass(Object o) {
        List<String> names = new ArrayList<String>();

        Class<?> classData = o.getClass();
        while (classData.getSuperclass()!=null) {
            names.add(classData.getSuperclass().getName());
            classData = classData.getSuperclass();
        }
        return names;
    }
}
