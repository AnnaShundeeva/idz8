
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;


public class ReflectionDemoTest {
    @Test
    public void findQuantityHumanTest() {
        Cat cat = new Cat();
        Human human = new Human();
        Human student = new Student();
        Human mathStudent = new MathStudent();

        List<Object> testList = new ArrayList<Object>();
        testList.add(cat);
        testList.add(human);
        testList.add(student);
        testList.add(mathStudent);

        int trueResult = 3;

        assertEquals(3, ReflectionDemo.findQuantityHuman(testList));
    }

    @Test
    public void getNameOpenMethodTest() {
        Cat cat = new Cat();

        List<String> resultList = new ArrayList();
        Method[] methods = cat.getClass().getMethods();
        for (Method method: methods) {
            resultList.add(method.getName());
        }

        assertEquals(resultList, ReflectionDemo.getNameOpenMethod(cat));
    }
    @Test
    public void getAllSuperclassTest() {
        Human mathStudent = new MathStudent();

        List<String> resultList = new ArrayList();
        resultList.add("Student");
        resultList.add("Human");
        resultList.add("java.lang.Object");

        assertEquals(resultList, ReflectionDemo.getAllSuperclass(mathStudent));
    }
}
